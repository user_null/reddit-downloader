## Welcome!
# This is the Reddit Image Downloader Script
---
## Usage

- To set up the folders to use it globally :
		$ ./rids new

- To set up the folders to use it into the current folder :
		$ ./rids new local

- Keep using it with :
		$ ./rids

- Index it in dmenu :
		# cp ./rids /usr/local/bin/

- Add new r/ in the subreddit.txt located in :

	- Globally ~/.config/rids/subreddit.txt
	- Locally ./subreddit.txt

* Try to always add an empty line at the end.

- Change configs in :

	- Globally ~/.config/rids/config
	- Locally ./config

* LIGHT MODE: Only .jpg and .png files.
* HEAVY MODE: .jpg, .png and .gifs, videos are not supported yet.

---
## Dependecies

- jq
- standard gnu coreutils
- wget
- bash?
---
## Next Features?
- Refactorization of the code only.

## Hotel?
- Trivago
